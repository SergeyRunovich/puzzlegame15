//
//  Extenshion.swift
//  PuzzleGame15
//
//  Created by Сергей Рунович on 24.12.20.
//
import Foundation
import UIKit

extension MenuScreenController {
    //MARK:- Functions -
    func tapStart() {
        let gesture = UITapGestureRecognizer(target: self,
                                             action: #selector(clickStartGameButton))
        pressStartGame.addGestureRecognizer(gesture)
        pressStartGame.isUserInteractionEnabled = true
        
    }
    @objc func clickedLabel() {
        if let url = URL(string: ClickLink.infoAboutGame.rawValue) {
            UIApplication.shared.open(url)
        }
    }
    func textSetting() {
        let nameTitleAttStrLabel = NSMutableAttributedString(string: " Welcome to the", attributes: [.foregroundColor: UIColor.cyan])
        nameTitleAttStrLabel.addAttribute(.font, value: UIFont(name: "Playlist-Script", size: 60) as Any, range: _NSRange(location: 0, length: nameTitleAttStrLabel.length))
        let nameAttStrLabel = NSMutableAttributedString(string: "Puzzle")
        
        nameAttStrLabel.addAttribute(.font, value: UIFont(name: "Hanging Letters", size: 85) as Any, range: _NSRange(location: 0, length: nameAttStrLabel.length))
        nameAttStrLabel.addAttribute(.foregroundColor, value: UIColor.green, range: _NSRange(location: 0, length: 1))
        nameAttStrLabel.addAttribute(.foregroundColor, value: UIColor.red, range: _NSRange(location: 1, length: 1))
        nameAttStrLabel.addAttribute(.foregroundColor, value: UIColor.yellow, range: _NSRange(location: 2, length: 1))
        nameAttStrLabel.addAttribute(.foregroundColor, value: UIColor.yellow, range: _NSRange(location: 3, length: 1))
        nameAttStrLabel.addAttribute(.foregroundColor, value: UIColor.red, range: _NSRange(location: 4, length: 1))
        nameAttStrLabel.addAttribute(.foregroundColor, value: UIColor.green, range: _NSRange(location: 5, length: 1))
        
        let infoAttrStrLabel = NSAttributedString(string: "About", attributes: [.link: ClickLink.infoAboutGame.rawValue])
        nameGameLabel.attributedText = nameTitleAttStrLabel
        aboutGameLabel.attributedText = infoAttrStrLabel
        puzzleNameLabel.attributedText = nameAttStrLabel
        
        let tapLink = UITapGestureRecognizer(target: self, action: #selector(clickedLabel))
        aboutGameLabel.isUserInteractionEnabled = true
        aboutGameLabel.addGestureRecognizer(tapLink)
        
    }
}

