//
//  GameController.swift
//  PuzzleGame15
//
//  Created by Сергей Рунович on 24.12.20.
//

import UIKit
enum ChooseLevelGame: Int {
    case easy = 3
    case medium = 4
    case hard = 5
    case expert = 6
}

struct Index {
    var row: Int
    var column: Int
}

@available(iOS 13.4, *)
@available(iOS 13.4, *)
class GameController: UIViewController {
    var counter = 0
    var hardLevel: [Int] = []
    var mediumLevel: [Int] = []
    var easyLevel: [Int] = []
    var expertLevel: [Int] = []
    var time = 0
    var timer : Timer?
    var isPaused = true
    var onFinish :  ((UIAlertController) -> ())?
    var currentGameLevel:ChooseLevelGame = .easy
    var emptyCell: GameBoardCellModel?
    var data: [[GameBoardCellModel]] = []
    var gameFinish: ((Bool) -> ())?
    let finish = true
    var playerTimerGame: ((String, String) -> ())?
    var levelGame = ""
    var gameConrtroller : GameController?
    
    
    
    //MARK:- IBOutletsView -
   
    @IBOutlet weak var backLabel: UIButton!
    @IBOutlet weak var timerLabel: UILabel!
    @IBOutlet weak var gameView: UIView!
    @IBOutlet weak var scoreLabel: UILabel!
    @IBOutlet weak var pauzeButtonOutlet: UIButton!
    @IBOutlet weak var namePlayer: UILabel!
    @IBOutlet weak var textFieldData: UITextField!
    
    
    
    
    
    
    
    //MARK:- ViewDidLoad -
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //dataPickerController()
        createAlert()
       
        backButtonLabelSettings()
        pauzePuttonSettings()
        gameView.alphaAndRadius()
        createGameViews()
        alertController.addAction(action)
      
        shuffleBoard()
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        
        updateBoard()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        timer?.invalidate()
        timer = nil
    }
    
   
//    func dataPickerController() {
//        let datePicker = UIDatePicker()
//        datePicker.datePickerMode = .date
//        datePicker.preferredDatePickerStyle = .wheels
//        datePicker.addTarget(self, action: #selector(hundlePicker(_:)), for: .valueChanged)
////        textFieldData.inputView = datePicker
//
//        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(hundleTap(_:)))
//        view.addGestureRecognizer(tapGesture)
//
//    }
//    @objc
//    func hundlePicker(_ sender : UIDatePicker) {
//     let dateFormatter = DateFormatter()
//        dateFormatter.dateFormat = "dd MMMM yyyy"
//        textFieldData.text = dateFormatter.string(from: sender.date)
//
//    }
//    @objc
//    func hundleTap(_ sender : UIDatePicker) {
//        textFieldData.resignFirstResponder()
//
//    }
    
    
    
    //MARK:- IBActions -
    @IBAction func finishGameButton(_ sender: UIButton) {
        getdataAndBack()
    }
    
    @IBAction func pauzeButton(_ sender: UIButton) {
        if sender.titleLabel?.text == "Pause"{
            sender.setTitle("Continue", for: .normal)
            sender.shadow(offset: CGSize.init(width: 3, height: 3), color: .blue, radius: 5, opacity: 1)
            timer?.invalidate()
            gameView.isUserInteractionEnabled = false
            gameView.alpha = 0.2       } else if sender.titleLabel?.text == "Continue" {
                sender.setTitle("Pause", for: .normal)
                sender.shadow(offset: CGSize.init(width: 3, height: 3), color: .blue, radius: 5, opacity: 1)
                startTimer()
                gameView.isUserInteractionEnabled = true
                gameView.alpha = 1
                
            }
    }
    
    @IBAction func backButton(_ sender: UIButton) {
        navigationController?.popViewController(animated: true)
    }
    

    
    
    
    
    //MARK:- Functions -
    
    
    let alertController = UIAlertController(title: "See you again!", message: ""  , preferredStyle: .alert)
    let action = UIAlertAction(title: "Ok", style: .default)
    func getdataAndBack() {
        navigationController?.popViewController(animated: true)
        if let action = self.onFinish {
            action(self.alertController)
        }
    }
    
    
    func createAlert() {
        
        namePlayer.isHidden = true
        let alert = UIAlertController(title: "Profile", message: "Enter a name", preferredStyle: .alert)
        alert.addTextField()
        let okAction = UIAlertAction(title: "Ok", style: .default) { [alert] _ in
            let answer = alert.textFields![0]
            if answer.text == "" {
                self.namePlayer.text = "Player 1"
            } else {
                self.namePlayer.text = answer.text
            }
            self.namePlayer.isHidden = false
            self.startTimer()
           
        }
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: { (handler) in
            self.namePlayer.text = "Player 1"
            self.namePlayer.isHidden = false
            self.startTimer()
        }))
        alert.addAction(okAction)
        
        
        present(alert, animated: true, completion: nil)
       
    }
    
    
    
    
    
    
    
    
    
//    MARK:- GameField -
    
    private func shuffle(){
        for _ in (0...200) {
            createGameViews()
        }
    }
    
    func createGameViews() {
     
        for row in 0..<currentGameLevel.rawValue {
            var rowArray: [GameBoardCellModel] = []
            for column in 1...currentGameLevel.rawValue {
                let currentCellNumber = row * currentGameLevel.rawValue + column
                var type: GameBoardCellType
                if currentCellNumber != currentGameLevel.rawValue * currentGameLevel.rawValue {
                    type = .full
                } else {
                    type = .empty
                }
                let cell = GameBoardCellModel(number: currentCellNumber, type: type)
                rowArray.append(cell)
                if type == .empty {
                    emptyCell = cell
                }
            }
            data.append(rowArray)
        }
    }
    func updateBoard() {
        
        for view in gameView.subviews {
            view.removeFromSuperview()
        }
        
        let size = gameView.frame.width / CGFloat(currentGameLevel.rawValue)
        
        
        var x: CGFloat = 0
        var y: CGFloat = 0
        
        for row in data {
            for cellModel in row {
                if let cell = UINib(nibName: "GameBoardCell", bundle: nil).instantiate(withOwner: self, options: nil).first as? GameBoardCell {
                    cell.frame = CGRect(x: x, y: y, width: size, height: size)
                    cell.setupCell(model: cellModel)
                    cell.setSetting()
                    cell.onAction = { [unowned self] in
                        self.moveEmptyCellToIndex(indexOfCell(cellModel))
                        self.checkIfGameIsOver()
                        counter += 1
                        scoreLabel.text = "Score: \(String(counter))"
                    }
                    
                  gameView.addSubview(cell)
                }
                x += size
            }
            x = 0
            y += size
        }
    }
    func swapTwoCells(_ firstCellModel: GameBoardCellModel, secondCellModel: GameBoardCellModel)
    {
        guard let firstIndex = indexOfCell(firstCellModel),
              let secondIndex = indexOfCell(secondCellModel) else {
            return
        }
        
        data[firstIndex.row][firstIndex.column] = secondCellModel
        data[secondIndex.row][secondIndex.column] = firstCellModel
        updateBoard()
    }
    
    func checkIfGameIsOver()
    {
        var numberOfCorrectElements = 0
        
        for row in 0..<currentGameLevel.rawValue {
            for column in 0..<currentGameLevel.rawValue {
                let currentCellNumber = row * currentGameLevel.rawValue + column + 1
                if currentCellNumber == data[row][column].number {
                    numberOfCorrectElements += 1
                }
            }
        }
        
        if numberOfCorrectElements == currentGameLevel.rawValue * currentGameLevel.rawValue {
            saveGameResults()
        }
    }
    func saveGameResults() {
        timer?.invalidate()
    gameView.alpha = 0.5
     gameView.isUserInteractionEnabled = false
        alertFinishGame(title: "Поздравляем!", message: "Время вашей игры \(timerLabel.text ?? ""), \n Колличество очков \(scoreLabel.text ?? "")") { (string) in
            self.gameView.alpha = 1
            self.pauzeButtonOutlet.isHidden = true
            
            self.dismiss(animated: true) {
                if let name = self.playerTimerGame {
                    if !string.isEmpty {
                        name("\(string) - \(self.levelGame)", self.timerLabel.text ?? "")
                    } else {
                        var namePlayer = string
                        namePlayer = "Default"
                        name("\(namePlayer) - \(self.levelGame)", self.timerLabel.text ?? "")
                    }
                }
            }
        }
    }
    
    func moveEmptyCellToIndex(_ index: Index?) {
        guard let emptyCell = emptyCell,
              let index = index,
              var emptyCellIndex = indexOfEmptyCell() else {
            return
        }
        
        if emptyCellIndex.row == index.row {
            let delta = emptyCellIndex.column > index.column ? -1 : 1
            while emptyCellIndex.column != index.column  {
                let indexToSwap = Index(row: emptyCellIndex.row, column: emptyCellIndex.column + delta)
                swapTwoCells(emptyCell, secondCellModel: data[indexToSwap.row][indexToSwap.column])
                emptyCellIndex = indexToSwap
            }
        } else if emptyCellIndex.column == index.column {
            let delta = emptyCellIndex.row > index.row ? -1 : 1
            while emptyCellIndex.row != index.row  {
                let indexToSwap = Index(row: emptyCellIndex.row + delta, column: emptyCellIndex.column)
                swapTwoCells(emptyCell, secondCellModel: data[indexToSwap.row][indexToSwap.column])
                emptyCellIndex = indexToSwap
            }
        }
    }
    
    func indexOfCell(_ cellModel: GameBoardCellModel?) -> Index? {
        guard let cellModel = cellModel else {
            return nil
        }
        for row in 0..<currentGameLevel.rawValue {
            for column in 0..<currentGameLevel.rawValue {
                let cell = data[row][column]
                if cell == cellModel {
                    return Index(row: row, column: column)
                }
            }
        }
        return nil
    }
    
    func indexOfEmptyCell() -> Index? {
        return indexOfCell(emptyCell)
    }

    // MARK: - Рандом
    
    private func indecesOnTheSameLinesWithEmpty() -> [Index] {
        guard let emptyIndex = indexOfEmptyCell() else {
            return []
        }
        
        var indeces: [Index] = []
        
        for index in 0..<currentGameLevel.rawValue {
            indeces.append(Index(row: index, column: emptyIndex.column))
            indeces.append(Index(row: emptyIndex.row, column: index))
        }
        
        return indeces
    }
    
    func shuffleBoard() {
        for _ in 0...100 {
            let randomPlaceToMove = indecesOnTheSameLinesWithEmpty().randomElement()
            moveEmptyCellToIndex(randomPlaceToMove)
        }
    }
    
}
    
    
    
    
    
    
    
    
    
    
    
    
    
