//
//  Extenshion + CreateView.swift
//  puzzleGame2020
//
//  Created by Сергей Рунович on 28.12.20.
//

import Foundation
import UIKit
extension GameController {
    func createSomeNewViews() {
        let newView = UIView()
        newView.frame = CGRect(x: x, y: y, width: viewWidht, height: viewHeight)
        let newLabel = UILabel()
        newLabel.frame = CGRect(x: 5, y: 5, width: 50, height: 50)
        
        newArray.shuffle()
        print(newArray)
        
        for value in 0..<counts {
            
            if x + viewWidht > gameView.frame.width {
                x = 0
                y += viewHeight
            }
            
            let newView = createView(x: x, y: y, width: viewWidht, height: viewHeight)
            
            if newView.frame.origin.y + newView.frame.height <= gameView.frame.height {
                gameView.addSubview(newView)
                newView.addSubview(createLabel(x: x, y: y, width: viewWidht, height: viewHeight, text: newArray[value] ))
                x += newView.frame.width
            } else {
                x = 0
                y = 0
            }
            
        }
    }
    func createLabel(x: CGFloat, y: CGFloat, width: CGFloat, height: CGFloat, text: Int) -> UILabel {
        let newLabel = UILabel()
        newLabel.text = String(text)
        newLabel.frame = CGRect(x: 0, y: 0, width: width, height: height)
        newLabel.textAlignment = .center
        return newLabel
    }
    
    func createView(x: CGFloat, y: CGFloat, width: CGFloat, height: CGFloat) -> UIView {
        let newView = UIView()
        newView.backgroundColor = .lightGray
        newView.addShadow()
        //newView.backgroundColor = UIColor(displayP3Red: red, green: green, blue: blue, alpha: 1)
        newView.frame = CGRect(x: x, y: y, width: viewWidht, height: viewHeight)
        return newView
        
    }
}
