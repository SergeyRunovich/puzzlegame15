//
//  GameBoardCellModel.swift
//  puzzleGame2020
//
//  Created by Сергей Рунович on 14.01.21.
//

import UIKit

enum GameBoardCellType {
    case empty
    case full
}

class GameBoardCellModel: NSObject {
    var number: Int
    var type: GameBoardCellType
    
    init(number: Int, type: GameBoardCellType) {
        self.number = number
        self.type = type
    }
}
