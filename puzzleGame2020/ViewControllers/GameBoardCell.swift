//
//  GameBoardCell.swift
//  puzzleGame2020
//
//  Created by Сергей Рунович on 14.01.21.
//

import UIKit

class GameBoardCell: UIView {
  
    override class func awakeFromNib() {
        let setting = UIView()
        
        setting.backgroundColor = .blue
    }
    @IBOutlet weak var numberLabel: UILabel!
    var onAction: (()->())?
    
    @IBAction func onButtonPress(_ sender: UIButton) {
        if let action = onAction {
            action()
        }
    }
    func setupCell(model: GameBoardCellModel) {
        numberLabel.text = String(model.number)
        if model.type == .empty {
            isUserInteractionEnabled = false
            alpha = 0
        }
    }
    func setSetting() {
        self.layer.cornerRadius = 20
        self.backgroundColor = .cyan
        
        numberLabel.font = UIFont(name:  "Playlist-Script", size: 25)
        
        
        self.layer.shadowColor = UIColor.green.cgColor
        self.layer.shadowOffset = .zero
        self.layer.shadowOpacity = 1
        self.layer.shadowPath = .none
    }
    
}
