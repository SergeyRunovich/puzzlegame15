//
//  ViewController.swift
//  PuzzleGame15
//
//  Created by Сергей Рунович on 24.12.20.
//

import UIKit



class MenuScreenController: UIViewController {
    var gameConrtroller : GameController?
    var results = false
    var arrayNamePlayer = UserDefaults.standard.array(forKey: "name") as? [String] ?? [String]()
    var arrayTimer = UserDefaults.standard.array(forKey: "time") as? [String] ?? [String]()
    
    
    //MARK:- Properties -
    enum ClickLink: String {
        case infoAboutGame = "https://ru.wikipedia.org/wiki/%D0%98%D0%B3%D1%80%D0%B0_%D0%B2_15"
    }
    
    
    //MARK:- IBOutlets -
    @IBOutlet weak var puzzleNameLabel: UILabel!
    @IBOutlet weak var nameGameLabel: UILabel!
    @IBOutlet weak var aboutGameLabel: UILabel!
    @IBOutlet weak var pressStartGame: UIImageView!
    
    
    
    
    
    //MARK: - LifeCycle -
    override func viewDidLoad() {
        super.viewDidLoad()
        tapStart()
        textSetting()
        
        
    }
    
    
    
    
    //MARK:- Functions -
    func pushGameController(level: ChooseLevelGame) {
        if self.gameConrtroller == nil || ((self.gameConrtroller) != nil) {
            var vc = storyboard?.instantiateViewController(identifier: "gameVC") as! GameController
           vc.playerTimerGame = { (name, time) in
                self.arrayNamePlayer.append(name)
                self.arrayTimer.append(time)
                UserDefaults.standard.setValue(self.arrayNamePlayer, forKey: "name")
                UserDefaults.standard.setValue(self.arrayTimer, forKey: "time")
            }
            vc.currentGameLevel = level
            vc.onFinish = { (Alert) in
                self.present(Alert, animated: true)
                
                  
            }
            vc.modalPresentationStyle = .fullScreen
            navigationController?.pushViewController(vc, animated: true)
           
        } else {
            navigationController?.pushViewController(self.gameConrtroller!, animated: true)
        }
    }
    
    
    //MARK:- IBActions -
    @IBAction func clickStartGameButton(_ gesture: UITapGestureRecognizer) {
        let alert = UIAlertController(title: "Choose a difficulty", message: "", preferredStyle: .actionSheet)
        alert.addAction(UIAlertAction(title: "3x3 (Easy level)", style: .default, handler: { (_) in
            self.pushGameController(level: .easy)
        }))
        alert.addAction(UIAlertAction(title: "4x4 (Medium level)", style: .default, handler: { (_) in
            self.pushGameController(level: .medium)
        }))
        alert.addAction(UIAlertAction(title: "5x5 (Hard level)", style: .default, handler: { (_) in
            self.pushGameController(level: .hard)
        }))
        alert.addAction(UIAlertAction(title: "6x6 (Expert level)", style: .default, handler: { (_) in
            self.pushGameController(level: .expert)
        }))
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    @IBAction func infoAction(_ sender: UIButton) {
        infoGame()
    }
    
}
extension MenuScreenController {
    
    func infoGame() {
        let newVC = storyboard?.instantiateViewController(withIdentifier: "InfoPlayerViewController") as! InfoPlayer
        newVC.modalPresentationStyle = .formSheet
        newVC.arrayNamePlayer = arrayNamePlayer
        newVC.arrayTimer = arrayTimer
        newVC.arrays = { (name, timer) in
            self.arrayNamePlayer = name
            self.arrayTimer = timer
        }
//        falseButtonIsSelected()
//        removeAllAnimated()
        
        present(newVC, animated: true, completion: nil)
    }
}


