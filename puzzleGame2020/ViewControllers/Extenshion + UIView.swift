//
//  Extenshion + UIView.swift
//  puzzleGame2020
//
//  Created by Сергей Рунович on 27.12.20.
//

import Foundation
import UIKit

extension UIView {
    //MARK:- Functions -
    func addShadow() {
        layer.shadowColor = UIColor.black.cgColor
        layer.shadowOffset = CGSize(width: 5, height: 5)
        layer.shadowOpacity = 1
    }
    func shadow(offset: CGSize, color: UIColor, radius: CGFloat, opacity: Float) {
        layer.shadowOffset = offset
        layer.shadowColor = color.cgColor
        layer.shadowRadius = radius
        layer.shadowOpacity = opacity
    }
    func alphaAndRadius() {
        backgroundColor = UIColor.white.withAlphaComponent(0.5)
        layer.cornerRadius = 10
    }
    
}
