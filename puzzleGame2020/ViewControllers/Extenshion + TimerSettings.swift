//
//  Extenshion + TimerSettings.swift
//  puzzleGame2020
//
//  Created by Сергей Рунович on 11.01.21.
//

import UIKit
extension GameController{
func startTimer() {
    isPaused = true
    timer?.invalidate()
    timer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(timerAction), userInfo: nil, repeats: true)
}

@objc func timerAction()  {
    if time > 0 {
        let minutes = time / 60
        let seconds = time % 60
        timerLabel.text = "\(minutes):\(seconds)"
    }
      time += 1
}
}
