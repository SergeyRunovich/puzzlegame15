//
//  Extenshion + GameControllerSettings.swift
//  puzzleGame2020
//
//  Created by Сергей Рунович on 11.01.21.
//
import UIKit
extension GameController {
    func alertFinishGame(title: String, message: String, with complition: @escaping (String) -> ()) {
      
        let alert = UIAlertController(title: title,
                                      message: message,
                                      preferredStyle: .alert)
        var alertTextField: UITextField!
        
        let okAction = UIAlertAction(title: "Сохранить",
                                     style: .default) { _ in
            complition(alertTextField.text ?? "Default")
            if let game = self.gameFinish {
                game(self.finish)
            }
            self.dismiss(animated: true, completion: nil)
            
        }
        
        let cancelAction = UIAlertAction(title: "Отмена",
                                         style: .destructive) {_ in
            self.gameView.alpha = 1
            self.timer?.invalidate()
            self.pauzeButtonOutlet.isHidden = true
            if let game = self.gameFinish {
                game(self.finish)
            }
        }
        
        alert.addAction(okAction)
        alert.addAction(cancelAction)
        
        alert.addTextField { textField in
            alertTextField = textField
        }
        
        present(alert, animated: true)
    }


func pauzePuttonSettings()  {
    pauzeButtonOutlet.shadow(offset: CGSize.init(width: 3, height: 3), color: .blue, radius: 5, opacity: 1)
}

func backButtonLabelSettings()  {
    backLabel.layer.cornerRadius = 10
}

}
