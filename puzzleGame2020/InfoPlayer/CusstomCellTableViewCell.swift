//
//  CusstomCellTableViewCell.swift
//  puzzleGame2020
//
//  Created by Сергей Рунович on 1.02.21.
//

import UIKit

class CusstomCellTableViewCell: UITableViewCell {

    @IBOutlet weak var numberOutlet: UILabel!
    @IBOutlet weak var namePlayer: UILabel!
    @IBOutlet weak var timerOutlet: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
